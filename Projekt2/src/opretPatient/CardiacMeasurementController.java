package opretPatient;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class CardiacMeasurementController {

    public void showCardiacMeasurement(javafx.event.ActionEvent event) throws Exception {
        Parent cardiacMeasurementParent = FXMLLoader.load(getClass().getResource("CardiacMeasurement.fxml"));
        Scene cardiacMeasurementScene = new Scene(cardiacMeasurementParent);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setTitle("Måling");
        window.setScene(cardiacMeasurementScene);
        window.show();
    }

    public void MainMenu(ActionEvent event) throws Exception {
        MainMenuController mainMenuView = new MainMenuController();
        mainMenuView.showMainMenu(event);
    }

    public void btnLogout(ActionEvent event) throws Exception {
        LoginController loginView = new LoginController();
        loginView.showLogin(event);
        }
}

