package opretPatient;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;



public class Main extends Application {

    @Override

    public void start(Stage window) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));
        Scene scene = new Scene(root);
        window.setTitle("Log ind");
        window.setScene(scene);
        window.show();
    }
        public static void main(String[] args) {
        launch(args);
    }
}

/** String username = "lissi";
 String password = "12345";
 String filepath = "sample/members.txt";

 verifyLogin(username, password, filepath);
 **/