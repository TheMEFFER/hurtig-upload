package opretPatient;

import java.io.*;
import java.util.Scanner;

//import com.sun.org.apache.xpath.internal.functions.FuncStringLength;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
//import sample.MainMenu2Controller;

public class LoginController {
    @FXML
    private javafx.scene.control.Label lblStatus;
    @FXML
    private TextField txtUserName;
    @FXML
    private TextField txtPassword;
    private String username = "";
    private String password = "";
    private String filepath = "C:\\Users\\meffe\\Desktop\\Projekt2\\src\\opretPatient\\members.txt";

    public void showLogin(javafx.event.ActionEvent event) throws Exception{
        Parent loginParent = FXMLLoader.load(getClass().getResource("Login.fxml"));
        Scene loginScene = new Scene(loginParent);
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        window.setTitle("Login");
        window.setScene(loginScene);
        window.show();
    }
    private static Scanner x;

    public void btnLogin(javafx.event.ActionEvent event) throws Exception {
        verifyLogin(username,password);
    } //, filepath

    private void verifyLogin(String username, String password) throws IOException {
        boolean found = false;
        String tempUsername = "";
        String tempPassword = "";

        //, String filepath
        //String leTempUserName = txtUserName.getText();


        if (txtUserName.getText().length() == 10) {
            filepath = "C:\\Users\\meffe\\Desktop\\Projekt2\\src\\opretPatient\\members2.txt";

            x = new Scanner(new File(filepath));
            x.useDelimiter(",");

            while (x.hasNext() && !found) {
                tempUsername = x.next();
                tempPassword = x.next();


                if (tempUsername.equals(txtUserName.getText()) && tempPassword.equals(txtPassword.getText())) {
                    Parent root = FXMLLoader.load(getClass().getResource("MainMenu2.fxml"));
                    Scene scene = new Scene(root);
                    Stage window = new Stage();
                    window.setTitle("Start menu for læge");
                    window.setScene(scene);
                    window.show();
                }
                else lblStatus.setText("Fejl i login, prøv venligst igen");
            }
        }
        else {
               // filepath = "C:\\Users\\meffe\\Desktop\\test1\\src\\sample\\members.txt";

            x = new Scanner(new File(filepath));
            x.useDelimiter(",");

            while (x.hasNext() && !found) {
                tempUsername = x.next();
                tempPassword = x.next();


                if (tempUsername.equals(txtUserName.getText()) && tempPassword.equals(txtPassword.getText())) {
                    Parent root = FXMLLoader.load(getClass().getResource("MainMenu.fxml"));
                    Scene scene = new Scene(root);
                    Stage window = new Stage();
                    window.setTitle("Start menu");
                    window.setScene(scene);
                    window.show();
                }
                else  lblStatus.setText("Fejl i login, prøv venligst igen");
            }
        }
    }
}



/**
 * Parent mainMenuParent = FXMLLoader.load(getClass().getResource("MainMenu.fxml"));
 *         Scene mainMenuScene = new Scene(mainMenuParent);
 *         //get the stage from main
 *         Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
 *         window.setScene(mainMenuScene);
 *         window.show();
 */

/**
 List<PatientListController> patient = Arrays.asList(
 new PatientListController("Charles", "Dickens", "Charles", "aaaa"),
 new PatientListController("Lewis", "Carroll", "Lewis","bbbb"),
 new PatientListController("Thomas", "Carlyle", "Thomas","cccc"),
 new PatientListController("Charlotte", "Bronte", "Charlotte","dddd"),
 new PatientListController("Matthew", "Arnold", "Mathew","eeee")
 );

 patient.stream()
 .filter(p -> p.getFirstName().startsWith("C"))
 .sorted(Comparator.comparing(PatientListController::getLastName))
 //.sorted((p1, p2)-> p1.getLastName().comopareTo(p2.getLastName())
 // .collect(Collectors.toList());
 .forEach(System.out::println);
 **/



/**public static class VerifyLogin {

   private static Scanner x;

   public static void main(String[] args){
    String username = "Lissi";
    String password = "12345";
    String filepath = "sample/members.txt";

    verifyLogin(username, password, filepath);

}

public static void verifyLogin(String username, String password, String filepath){
    boolean found = false;
    String tempUserName = "";
    String tempPassword = "";

    try{
        x = new Scanner(new File(filepath));
        x.useDelimiter("[,\n]");

        File temp = new File("members.txt");
        Scanner file = new Scanner(temp);

        while(x.hasNext()&& !found){
            tempUserName = x.next();
            tempPassword = x.next();

            if(tempUserName.trim().equals(username.trim()) && tempPassword.trim().equals(password.trim()))
            {
                found = true;
            }
        }
        x.close();
        System.out.println(found);
    }
    catch(Exception e){
        System.out.println("den her error");
    }
}
**/


//}
/**
 if (txtUserName.getText().equals("user") && txtPassword.getText().equals("pass")){
 MainMenuController mainMenuView = new MainMenuController();
 mainMenuView.showMainMenu(event);
 } else{
 lblStatus.setText("Fejl i login, prøv venligst igen");
 }
 **/
/**

 try {
         x = new Scanner(new File(filepath));
         x.useDelimiter(",");

         while (x.hasNext() && !found) {
         tempUsername = x.next();
         tempPassword = x.next();

         if (tempUsername.equals(txtUserName.getText()) && tempPassword.equals(txtPassword.getText())) {
         // MainMenuController mainMenuView = new MainMenuController();
         //mainMenuView.showMainMenu();
         Parent root = FXMLLoader.load(getClass().getResource("MainMenu.fxml"));
         Scene scene = new Scene(root);
         Stage stage = new Stage();
         stage.setTitle("Log ind");
         stage.setScene(scene);
         stage.show();
         }
         }
         x.close();
         System.out.println(found);
         }
         catch (Exception e){
         lblStatus.setText("Fejl i login, prøv venligst igen");
         }
 **/