package opretPatient;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainMenu2Controller {

    public void showMainMenu(javafx.event.ActionEvent event) throws Exception {
        Parent mainMenuParent = FXMLLoader.load(getClass().getResource("MainMenu2.fxml"));
        Scene mainMenuScene = new Scene(mainMenuParent);
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        window.setTitle("Start menu for læge");
        window.setScene(mainMenuScene);
        window.show();
    }

    public void btnCardiacMeasurement(javafx.event.ActionEvent event) throws Exception {
        PersonOverviewController createPatientView = new PersonOverviewController();
        createPatientView.showCreatePatient(event);
    }

    public void btnLogout(javafx.event.ActionEvent event) throws Exception {
        LoginController loginView = new LoginController();
        loginView.showLogin(event);
    }
}