package opretPatient;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainMenuController {

    public void showMainMenu(javafx.event.ActionEvent event) throws Exception {
        Parent mainMenuParent = FXMLLoader.load(getClass().getResource("MainMenu.fxml"));
        Scene mainMenuScene = new Scene(mainMenuParent);
        Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
        window.setTitle("Start menu");
        window.setScene(mainMenuScene);
        window.show();
    }

    public void btnCardiacMeasurement(javafx.event.ActionEvent event) throws Exception {
        CardiacMeasurementController cardiacMeasurementView = new CardiacMeasurementController();
        cardiacMeasurementView.showCardiacMeasurement(event);
    }

    public void btnLogout(javafx.event.ActionEvent event) throws Exception {
        LoginController loginView = new LoginController();
        loginView.showLogin(event);
    }
}